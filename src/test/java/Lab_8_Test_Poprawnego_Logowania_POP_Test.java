import config.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_8_Test_Poprawnego_Logowania_POP_Test extends SeleniumBaseTest{
    
    @Test
    public void correctLoginTest() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail(config.getApplicationUser());
        loginPage.typePassword(config.getApplicationPassword());
        HomePage homePage = loginPage.submitLogin();
        homePage.assertWelcomeElementIsShown();
    }

    @Test
    public void correctLoginTestWithChaining() {
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .assertWelcomeElementIsShown();
    }

}
