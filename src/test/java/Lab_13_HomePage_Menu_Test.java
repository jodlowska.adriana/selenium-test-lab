import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_13_HomePage_Menu_Test extends SeleniumBaseTest  {

    @Test
    public void gotToProcessesTest(){
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses();
    }

    @Test
    public void goToDashboards(){
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToDashboards();
    }

    @Test
    public void goToCharacteristics(){
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics();
    }
}

