import config.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_9_Test_Niepoprawnego_Logowania_POP_Test extends SeleniumBaseTest {

    @Test
    public void incorrectLoginTestWithChaining() {
        new LoginPage(driver)
                .typeEmail("test2@test.com")
                .typePassword(config.getApplicationPassword())
                .submitLoginWithFailure()
                .assertLoginErrorsIsShown();
    }

}
