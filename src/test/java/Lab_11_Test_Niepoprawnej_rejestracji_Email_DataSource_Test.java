import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.RegisterPage;

import java.util.concurrent.TimeUnit;

public class Lab_11_Test_Niepoprawnej_rejestracji_Email_DataSource_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailRegisterTestWithChaining(String wrongEmail) {

        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail(wrongEmail)
                .submitRegisterWithFailure()
                .assertRegisterEmailErrorsIsShown();

    }

}
