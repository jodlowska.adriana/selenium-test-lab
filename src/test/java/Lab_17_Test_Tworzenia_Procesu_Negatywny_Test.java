import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.ProcessesPage;
import pages.CreateProcessPage;

import java.util.UUID;

public class Lab_17_Test_Tworzenia_Procesu_Negatywny_Test extends SeleniumBaseTest{

    @Test
    public void addProcessTestWithFailureTest(){
        String shortProcessName = "ab";
        String expErrorMessage = "The field Name must be a string with a minimum length of 3 and a maximum length of 30.";
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(shortProcessName)
                .submitCreateWithFailure()
                      .assertErrorMessage(expErrorMessage)
                .backToList()
                      .assertProcessIsNotShown(shortProcessName);

    }
}
