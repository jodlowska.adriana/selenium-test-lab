package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultsPage extends HomePage{

    public ResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[contains(text(),'Add results sample')]")
    private WebElement addResults;

    @FindBy(xpath = "//a[contains(text(),'Back to characteristic')]")
    private WebElement backToCharacteristicBtn;

    public CreateResultsPage clickAddResults() {
        addResults.click();
        return new CreateResultsPage(driver);
    }

    public CharacteristicsPage backToCharacteristicPage(){
        backToCharacteristicBtn.click();
        return new CharacteristicsPage(driver);
    }
}
