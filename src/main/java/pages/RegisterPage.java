package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class RegisterPage {
    protected WebDriver driver;

    public RegisterPage (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement RegisterBtn;

    @FindBy(id = "Email-error")
    public WebElement emailError;

    @FindBy(css = "a[href*=Login]")
    private WebElement loginLnk;

    public RegisterPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public RegisterPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public RegisterPage typeConfirmPassword(String confirmPassword){
        emailTxt.clear();
        emailTxt.sendKeys(confirmPassword);
        return this;
    }

    public HomePage submitRegister() {
        RegisterBtn.click();
        return new HomePage(driver);
    }
    public RegisterPage submitRegisterWithFailure() {
        RegisterBtn.click();
        return this;
    }
    public LoginPage goToLoginPage() {
        loginLnk.click();
        return new LoginPage(driver);
    }
    public RegisterPage assertRegisterEmailErrorsIsShown() {
        Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");
        return this;
    }
}
