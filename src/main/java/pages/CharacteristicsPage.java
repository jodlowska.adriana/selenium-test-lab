package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

public class CharacteristicsPage extends HomePage {

    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";
    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }
    private final String GENERIC_CHARACTERISTIC_ROW_XPATH = "//td[text()='%s']/..";

    @FindBy(css = ".title_left")
    private WebElement characteristicsElm;

    @FindBy(css = ".btn-success")
    private WebElement addCharacteristic;

    @FindBy(css = ".btn-success")
    private WebElement resultBtn;


    public CharacteristicsPage assertCharacteristicsHeader() {
        Assert.assertTrue(characteristicsElm.isDisplayed(), "Characteristics element is not shown.");
        Assert.assertTrue(characteristicsElm.getText().contains("Characteristics"), "Characteristics element text: '" + characteristicsElm.getText() + "' does not contain word 'Characteristics'");
        return this;
    }


    public CharacteristicsPage assertCharacteristicsUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
        return this;
    }
    public CreateCharacteristicPage clickAddCharacteristic() {
        addCharacteristic.click();
        return new CreateCharacteristicPage(driver);
    }



    public CharacteristicsPage assertCharacteristic(String expName, String expLsl, String expUsl, String expBinCount) {
        String characteristicXpath = String.format(GENERIC_CHARACTERISTIC_ROW_XPATH, expName);
        WebElement characteristicRow = driver.findElement(By.xpath(characteristicXpath));
        String actLsl = characteristicRow.findElement(By.xpath("./td[3]")).getText();
        String actUsl = characteristicRow.findElement(By.xpath("./td[4]")).getText();
        String actBinCount = characteristicRow.findElement(By.xpath("./td[5]")).getText();
        Assert.assertEquals(actLsl, expLsl);
        Assert.assertEquals(actUsl, expUsl);
        Assert.assertEquals(actBinCount, expBinCount);
        return this;
    }

    public CharacteristicsPage assertCharacteristicIsNotShown(String characteristicName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, characteristicName);
        List<WebElement> process = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);
        return this;
    }
    private String GENERIC_CHARACTERISTIC_RESULTS_XPATH = "//td[text()='%s']/..//a[contains(@href, 'Results')]";

    public ResultsPage goToResults(String characteristicName){
        String resultsBtnXpath = String.format(GENERIC_CHARACTERISTIC_RESULTS_XPATH, characteristicName);
        driver.findElement(By.xpath(resultsBtnXpath)).click();

        return new ResultsPage(driver);
    }

    private String GENERIC_CHARACTERISTIC_REPORT_XPATH = "//td[text()='%s']/..//a[contains(@href, 'Report')]";

    public ReportPage goToReport(String characteristicName){
        String reportBtnXpath = String.format(GENERIC_CHARACTERISTIC_REPORT_XPATH, characteristicName);
        driver.findElement(By.xpath(reportBtnXpath)).click();

        return new ReportPage(driver);
    }


}
