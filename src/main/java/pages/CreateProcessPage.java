package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import javax.naming.Name;

public class CreateProcessPage {
   protected WebDriver driver;


    public CreateProcessPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#Name")
    private WebElement processNameElm;

    @FindBy(css = "#Description")
    private WebElement processDescriptionElm;

    @FindBy(css = "#Notes")
    private WebElement processNotesElm;

    @FindBy(css = ".btn-success")
    private WebElement createElm;

    @FindBy(css = ".flash-message")
    private WebElement flashMessage;

    @FindBy(css = ".btn-success")
    private WebElement create;

    @FindBy(css = ".field-validation-error")
    private WebElement expErrorMessage;

    @FindBy(xpath = "//a[contains(text(),'Back to List')]")
    private WebElement backToListBtn;


    public ProcessesPage submitCreate() {
        createElm.click();
        return new ProcessesPage(driver);
    }
    public CreateProcessPage typeName(String expName) {
        processNameElm.clear();
        processNameElm.sendKeys(expName);
        return this;
    }

    public CreateProcessPage typeDescription(String expDescription) {
        processDescriptionElm.clear();
        processDescriptionElm.sendKeys(expDescription);
        return this;
    }

    public CreateProcessPage typeNotes(String expNotes) {
        processNotesElm.clear();
        processNotesElm.sendKeys(expNotes);
        return this;
    }

    public CreateProcessPage assertErrorMessage(String expectedErrorMessage) {

        Assert.assertTrue(expErrorMessage.isDisplayed(), "Error message is not shown.");
        Assert.assertTrue(expErrorMessage.getText().equals(expectedErrorMessage), "Error message element text: '" + expErrorMessage.getText() + "' differs from '" + expectedErrorMessage);
        return this;

    }

    public CreateProcessPage submitCreateWithFailure(){
//        Assert.assertTrue(create.isDisplayed(), "Create button is not shown.");
        create.click();
        return this;
    }

    public ProcessesPage backToList(){
        backToListBtn.click();
        return new ProcessesPage(driver);
    }
}

