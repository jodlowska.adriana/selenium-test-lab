package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

public class CreateCharacteristicPage {
    protected WebDriver driver;


    public CreateCharacteristicPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "ProjectId")
    private WebElement projectSlc;

    @FindBy(id = "Name")
    private WebElement nameTxt;

    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowerTxt;

    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperTxt;

    @FindBy(id = "HistogramBinCount")
    private WebElement histogramTxt;

    @FindBy(css = ".btn-success")
    private WebElement createCharacteristicBtn;

    @FindBy(css = ".field-validation-error")
    private WebElement errorMessage;

    @FindBy(xpath = "//a[contains(text(),'Back to List')]")
    private WebElement backToListBtn;

    public CreateCharacteristicPage typeName(String name) {
        nameTxt.clear();
        nameTxt.sendKeys(name);
        return this;
    }

    public CreateCharacteristicPage typeLsl(String lower) {
        lowerTxt.clear();
        lowerTxt.sendKeys(lower);
        return this;
    }

    public CreateCharacteristicPage typeUsl(String upper) {
        upperTxt.clear();
        upperTxt.sendKeys(upper);
        return this;
    }

    public CreateCharacteristicPage typeHistogram(String histogram) {
        histogramTxt.clear();
        histogramTxt.sendKeys(histogram);
        return this;
    }

    public CreateCharacteristicPage selectProcess(String processName) {
        new Select(projectSlc).selectByVisibleText(processName);
        return this;
    }

    public CharacteristicsPage submitCreate(){
    createCharacteristicBtn.click();
    return new CharacteristicsPage(driver);
    }
    public CreateCharacteristicPage submitCreateWithFailure(){
        createCharacteristicBtn.click();
        return this;
    }

    public CreateCharacteristicPage assertErrorMessage(String expectedErrorMessage) {

        Assert.assertTrue(errorMessage.isDisplayed(), "Error message is not shown.");
        Assert.assertTrue(errorMessage.getText().equals("The value 'Select process' is not valid for ProjectId."), "Error message element text: '" + errorMessage.getText() + "' differs from '" + expectedErrorMessage);
        return this;

    }


    public CharacteristicsPage backToList(){
        backToListBtn.click();
        return new CharacteristicsPage(driver);
    }

}
