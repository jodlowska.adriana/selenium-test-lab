package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.awt.*;
import java.util.List;

public class LoginPage {

    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = ".validation-summary-errors>ul>li")
    private List<WebElement> loginErrors;

    @FindBy(id = "Email-error")
    public WebElement emailError;

    @FindBy(css = "a[href*=Register]")
    private WebElement registerLnk;


    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }


    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public LoginPage submitLoginWithFailure() {
        loginBtn.click();
        return this;
    }

    public RegisterPage goToRegisterPage() {
        registerLnk.click();
        return new RegisterPage(driver);
    }

    public LoginPage assertLoginErrorsIsShown() {
        Assert.assertEquals(loginErrors.get(0).getText(), "Invalid login attempt.");
        return this;
    }
    public LoginPage assertEmailErrorsIsShown() {
     Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");
        return this;
    }
}


