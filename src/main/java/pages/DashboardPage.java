package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardPage extends HomePage {

    public DashboardPage  (WebDriver driver) {
      super(driver);
    }

    @FindBy (css = ".x_title")
    private WebElement dashboardElm;

    public DashboardPage assertDemoProjectIsShown() {
        Assert.assertTrue(dashboardElm.isDisplayed(), "DEMO PROJECT element is not shown.");
        Assert.assertTrue(dashboardElm.getText().contains("DEMO PROJECT"), "DEMO PROJECT element text: '" + dashboardElm.getText() + "' does not contain word 'DEMO PROJECT'");
        return this;
    }

    public DashboardPage assertDashboardsUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
        return this;
    }

}
