package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ProcessesPage extends HomePage{

    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";
    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".title_left")
    private WebElement processesElm;

    @FindBy(css = ".btn-success")
    private WebElement addProcess;



    public ProcessesPage assertProcessesElementIsShown() {
        Assert.assertTrue(processesElm.isDisplayed(), "Processes element is not shown.");
        Assert.assertTrue(processesElm.getText().contains("Processes"), "Processes element text: '" + processesElm.getText() + "' does not contain word 'Processes'");
        return this;
    }

    public ProcessesPage assertProcessIsNotShown(String processName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, processName);
        List<WebElement> process = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);
        return this;
    }

    public ProcessesPage assertProcessesUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
        return this;
    }



    public ProcessesPage assertProcess(String expName, String expDescription, String expNotes) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);

        WebElement processRow = driver.findElement(By.xpath(processXpath));

        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();

        Assert.assertEquals(actDescription, expDescription);
        Assert.assertEquals(actNotes, expNotes);

        return this;
    }
    public CreateProcessPage clickAddProcess() {
        addProcess.click();
        return new CreateProcessPage(driver);
    }


}
